const messages = {
  "zh-CN": {
    home: "首页",
    projectfeature: "项目特色",
    experiment: "进入实验",
    projectTeam: "项目团队",
    projectSkill: "项目技术研发",
    projectNetwork: "网络相关要求",
    downloadCenter: "下载中心",
    experimentRead: "批阅中心",
    adminVerify: "管理员审批",
    STUDENT: "学生",
    TEACHER: "教师",
    SYSTEM_ADMIN: "管理员",
    type: "类型"
  }
};
export default messages;
