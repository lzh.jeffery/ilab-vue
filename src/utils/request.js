import axios from "axios";
import auth from "@/utils/auth";
import store from "@/store";

const unless = ["/accounts/login"];

// 创建axios实例
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API || "/api", // api 的 base_url
  timeout: 5000 // 请求超时时间
});

// request拦截器

service.interceptors.request.use(
  async config => {
    if (auth.token && unless.indexOf(config.url) === -1) {
      if (auth.checkTokenIsExpired()) {
        let { username, password } = auth.userMessageBase64decode();
        if (!username || !password) throw new Error("缺少用户重新登陆信息");
        await store.dispatch("accounts/login", {
          username,
          password
        });
      }
      config.headers["authorization"] = `Bearer ${auth.token}`; // 让每个请求携带自定义token 请根据实际情况自行修改
    }
    return config;
  },
  error => {
    // Do something with request error
    if (process.env.NODE_ENV === "development") console.log(error); // for debug
    Promise.reject(error);
  }
);

// respone拦截器
service.interceptors.response.use(
  response => {
    return response.data;
  },
  error => {
    if (process.env.NODE_ENV === "development") console.error(error);
    return Promise.reject(error.response.data);
  }
);

export default service;
