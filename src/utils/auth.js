import { AUTH } from "../const";
import { Base64 } from "js-base64";

class Auth {
  get token() {
    return localStorage.getItem(AUTH.TOKEN);
  }

  get user() {
    let _user = localStorage.getItem(AUTH.USER_DETAIL);
    if (!_user) return null;
    return JSON.parse(Base64.decode(_user));
  }

  constructor() {}

  /**
   *
   * @param {string} _token
   */
  setToken(_token) {
    localStorage.setItem(AUTH.TOKEN, _token);
  }

  userDataBase64encode(user) {
    let hash = Base64.encode(JSON.stringify(user));
    localStorage.setItem(AUTH.USER_DETAIL, hash);
  }

  /**
   *
   * @param {string} username
   * @param {string} password
   */
  userMessageBase64encode(username, password) {
    let hash = Base64.encode(`${username}.${Base64.encode(password)}`);
    localStorage.setItem(AUTH.U_MESSAGE, hash);
  }

  /**
   *
   * @return { username: string, password: string }
   */
  userMessageBase64decode() {
    let U_MESSAGE = localStorage.getItem(AUTH.U_MESSAGE);
    if (!U_MESSAGE) return null;
    let [username, passwordHash] = Base64.decode(U_MESSAGE).split(".");
    let password = Base64.decode(passwordHash);
    return {
      username,
      password
    };
  }

  checkTokenIsExpired() {
    let _token = this.token;
    console.log(_token);
    if (!_token) return true;
    let result = _token.split(".");
    let payload = result[1];
    let { exp } = JSON.parse(Base64.decode(payload));
    let now = parseInt(+new Date() / 1000, 10);
    return now - exp > AUTH.TIMEOUT - 60 * 30;
  }
}

export default new Auth();
