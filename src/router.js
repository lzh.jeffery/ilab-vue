import Vue from "vue";
import Router from "vue-router";
import Layout from "./views/layout";
import auth from "@/utils/auth";
import store from "@/store";
import { Message } from "element-ui";

Vue.use(Router);

function getUserRoles() {
  let roles =
    store.getters["accounts/user"] === null
      ? []
      : store.getters["accounts/user"].roles
      ? store.getters["accounts/user"].roles
      : [];
  return roles.map(role => role.name);
}

function checkPermission(canPassRoles) {
  let roles = getUserRoles();
  if (roles.length === 0) return false;
  if (process.env.NODE_ENV === "development") {
    let isAdmin = roles.find(role => role === "SYSTEM_ADMIN");
    if (isAdmin) return true;
  }
  let canPass = roles.some(
    role => canPassRoles.findIndex(val => val === role) !== -1
  );
  return canPass;
}

export const constantRouterMap = [
  {
    path: "/registered",
    component: () => import("@/views/registered"),
    hidden: true,
    beforeEnter: (to, from, next) => {
      if (auth.token && !auth.checkTokenIsExpired()) next("/");
      else next();
    }
  },
  {
    path: "/login",
    component: () => import("@/views/login"),
    hidden: true,
    beforeEnter: (to, from, next) => {
      if (auth.token && !auth.checkTokenIsExpired()) next("/");
      else next();
    }
  },
  // { path: "/404", component: () => import("@/views/404"), hidden: true },
  {
    path: "/",
    component: Layout,
    children: [
      {
        path: "",
        name: "home",
        component: () => import("@/views/home")
      }
    ]
  },
  {
    path: "/project-feature",
    component: Layout,
    children: [
      {
        path: "",
        name: "projectfeature",
        component: () => import("@/views/projectFeature")
      }
    ]
  },
  {
    path: "/experiment",
    component: Layout,
    children: [
      {
        path: "",
        name: "experiment",
        redirect: "/experiment-objective",
        component: () => import("@/views/experiment"),
        children: [
          {
            path: "/experiment-objective",
            name: "experimentobjective",
            component: () => import("@/views/experiment/experimentObjective")
          },
          {
            path: "/experiment-principle",
            name: "experimentprinciple",
            component: () => import("@/views/experiment/experimentPrinciple")
          },
          {
            path: "/experiment-devices",
            name: "experimentdevices",
            component: () => import("@/views/experiment/experimentDevices")
          },
          {
            path: "/experiment-material",
            name: "experimentmaterial",
            component: () => import("@/views/experiment/experimentMaterial")
          },
          {
            path: "/experiment-function",
            name: "experimentfunction",
            component: () => import("@/views/experiment/experimentFunction")
          },
          {
            path: "/experiment-steps",
            name: "experimentsteps",
            component: () => import("@/views/experiment/experimentSteps")
          },
          {
            path: "/experiment-result",
            name: "experimentresult",
            component: () => import("@/views/experiment/experimentResult")
          },
          {
            path: "/experiment-requires",
            name: "experimentrequires",
            component: () => import("@/views/experiment/experimentRequires")
          },
          {
            path: "/experiment-examination",
            name: "experimentExamination",
            component: () => import("@/views/experiment/examination")
          },
          {
            path: "/experiment-report",
            name: "experimentreport",
            component: () => import("@/views/experiment/experimentReport")
          }
        ]
      }
    ]
  },
  {
    path: "/project-team",
    component: Layout,
    children: [
      {
        path: "",
        name: "projectTeam",
        component: () => import("@/views/projectTeam")
      }
    ]
  },
  {
    path: "/project-skill",
    component: Layout,
    children: [
      {
        path: "",
        name: "projectSkill",
        component: () => import("@/views/projectSkill")
      }
    ]
  },
  {
    path: "/project-network",
    component: Layout,
    children: [
      {
        path: "",
        name: "projectNetwork",
        component: () => import("@/views/projectNetwork")
      }
    ]
  },
  {
    path: "/download-center",
    component: Layout,
    children: [
      {
        path: "",
        name: "downloadCenter",
        component: () => import("@/views/downloadCenter")
      }
    ]
  },
  {
    path: "/experiment-read",
    component: Layout,
    hidden: () => !checkPermission(["TEACHER"]),
    beforeEnter: (to, from, next) => {
      if (!checkPermission(["TEACHER"])) {
        Message({
          type: "warning",
          message: "访问该页面权限不足",
          duration: 1000
        });
        return next(false);
      }
      return next();
    },
    children: [
      {
        path: "",
        name: "experimentRead",
        component: () => import("@/views/experimentRead")
      }
    ]
  },
  {
    path: "/admin-verify",
    component: Layout,
    hidden: () => !checkPermission(["SYSTEM_ADMIN"]),
    // hidden: () => {
    //   switch (true) {
    //     case store.getters["accounts/user"] === null:
    //       return true;
    //     case store.getters["accounts/user"].roles.findIndex(
    //       role => role.name === "SYSTEM_ADMIN"
    //     ) === -1:
    //       return true;
    //   }
    //   return false;
    // },
    children: [
      {
        path: "",
        name: "adminVerify",
        component: () => import("@/views/adminVerify"),
        beforeEnter: (to, from, next) => {
          if (!checkPermission(["SYSTEM_ADMIN"])) {
            Message({
              type: "warning",
              message: "访问该页面权限不足",
              duration: 1000
            });
            return next(false);
          }
          return next();
        }
      }
    ]
  }
];

export default new Router({
  mode: "history",
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
});
