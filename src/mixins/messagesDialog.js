export default {
  methods: {
    successDialog(data) {
      this.$message({
        message: data || "保存成功！",
        type: "success"
      });
    },
    errorDialog(data) {
      this.$message.error(data || "保存失败！");
    }
  }
};
