import request from "@/utils/request";
import Resource from "./resource";
import { Base64 } from "js-base64";

let _roles = new Resource("roles");
let _organizations = new Resource("organizations");
let _accounts = new Resource("accounts");
let _lessons = new Resource("lessons");
let _labs = new Resource("labs");
let _answersheets = new Resource("answersheets");

_accounts.login = async (username, password, config = {}) => {
  let requestConfig = Object.assign(config, {
    url: "/accounts/login",
    method: "post",
    data: {
      identity: username,
      password: Base64.encode(password)
    }
  });
  return request(requestConfig);
};

_answersheets.pass = async (data, config = {}) => {
  let requestConfig = Object.assign(config, {
    url: "/answersheets/pass",
    method: "post",
    data
  });
  return request(requestConfig);
};

export { _roles as roles };
export { _organizations as organizations };
export { _accounts as accounts };
export { _lessons as lessons };
export { _labs as labs };
export { _answersheets as answersheets };
