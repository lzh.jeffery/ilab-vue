import request from "@/utils/request";
import util from "util";
import _ from "underscore";

const METHODS = {
  list: (resource, params) => {
    return request({
      url: `/${resource}`,
      method: "get",
      params
    });
  },
  create: (resource, data, option) => {
    let requestConfig = Object.assign(option, {
      url: `/${resource}`,
      method: "post",
      data
    });
    return request(requestConfig);
  },
  update: (resource, item) => {
    return request({
      url: `/${resource}/${item.id}`,
      method: "put",
      data: _.omit(item, "id")
    });
  },
  updateAll: (resource, params, data) => {
    return request({
      url: `/${resource}`,
      method: "put",
      params,
      data
    });
  },
  detail: (resource, id, params) => {
    const url = id ? `/${resource}/${id}` : `/${resource}`;
    return request({
      url,
      method: "get",
      params
    });
  },
  delete: (resource, id) => {
    return request({
      url: `/${resource}/${id}`,
      method: "delete"
    });
  },
  deleteAll: (resource, params) => {
    return request({
      url: `/${resource}`,
      method: "delete",
      params
    });
  }
};

class Resource {
  constructor(name) {
    this._name = name;
  }

  async _makeRequest(method, ...args) {
    console.info(
      `${method} ${this._name} with args ${util.inspect(args, {
        depth: null
      })}`
    );
    return METHODS[method](this._name, ...args);
  }
  async list(filters) {
    return this._makeRequest("list", filters);
  }

  async create(data, option = {}) {
    return this._makeRequest("create", data, option);
  }

  async update(item) {
    return this._makeRequest("update", item);
  }

  async updateAll(filters, data) {
    return this._makeRequest("updateAll", filters, data);
  }

  async detail(id, filters) {
    return this._makeRequest("detail", id, filters);
  }

  async delete(id) {
    return this._makeRequest("delete", id);
  }

  async deleteAll(filters) {
    return this._makeRequest("deleteAll", filters);
  }
}

export default Resource;
