import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";

import "normalize.css/normalize.css"; // A modern alternative to CSS resets

import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import locale from "element-ui/lib/locale/lang/en"; // lang i18n
import VueObserveVisibility from "vue-observe-visibility";
import AudioRecorder from "vue-audio-recorder";

Vue.use(AudioRecorder);

import VueI18n from "vue-i18n";
import messages from "./i18n";

import "@/styles/index.scss"; // global css

Vue.use(ElementUI, { locale });
Vue.use(VueI18n);
Vue.use(VueObserveVisibility);

Vue.config.productionTip = false;

const i18n = new VueI18n({
  locale: "zh-CN",
  messages
});

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");
