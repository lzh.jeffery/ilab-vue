import Vue from "vue";
import Vuex from "vuex";
import accounts from "./modules/accounts";
import app from "./modules/app";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    accounts,
    app
  }
});

export default store;
