import auth from "@/utils/auth";
import { accounts as _accounts } from "@/api";

const accounts = {
  namespaced: true,
  state: {
    user: auth.user,
    token: auth.token,
    isExpert: false
  },
  getters: {
    token: state => state.token,
    user: state => state.user,
    isLogin: state => state.user !== null,
    isExpert: state => state.isExpert
  },
  mutations: {
    refresh(state) {
      state.user = auth.user;
      state.token = auth.token;
      state.isExpert = false;
    },
    expertLogin(state) {
      state.isExpert = true;
    }
  },
  actions: {
    async login({ commit }, userForm) {
      let result = await _accounts.login(userForm.username, userForm.password);
      if (result.token) {
        auth.setToken(result.token);
        auth.userMessageBase64encode(userForm.username, userForm.password);
        if (result.user) auth.userDataBase64encode(result.user);
      }
      commit("refresh");
      return true;
    },
    async logout({ commit }) {
      localStorage.clear();
      commit("refresh");
    }
  }
};
export default accounts;
