import { lessons } from "@/api";

const app = {
  namespaced: true,
  state: {
    lessonTotal: 0,
    lessons: []
  },
  getters: {
    lessons: state => state.lessons
  },
  mutations: {
    setLessons(state, datas) {
      state.lessons = datas.items;
      state.lessonTotal = datas.total;
    }
  },
  actions: {
    async fetchLessons({ commit }) {
      let result = await lessons.list();
      commit("setLessons", result);
      return true;
    }
  }
};

export default app;
